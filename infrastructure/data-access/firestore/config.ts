import * as admin from 'firebase-admin';

import { ILogger } from '../../../logger/Logger';
import CommonRepository from './CommonRepository';
import { ICommonRepository } from './ICommonRepository';

export type firebaseConfig = {
  file: string;
};

export enum COLLECTIONS_NAMES {
  TASKS = 'tasks',
  AUTH = 'roles',
  USERS = 'users'
}

export function openFirebaseConnection(firebase: firebaseConfig, logger: ILogger): void {
  const db = admin.initializeApp({
    credential: admin.credential.cert(firebase.file)
  });
  //Collections
  const tasksCollection = db.firestore().collection(COLLECTIONS_NAMES.TASKS);
  const authCollection = db.firestore().collection(COLLECTIONS_NAMES.AUTH);
  const userCollection = db.firestore().collection(COLLECTIONS_NAMES.USERS);
  //ORM
  tasksFirebase = CommonRepository<any>(tasksCollection, logger.child({ repository: 'task' }));
  authFirebase = CommonRepository<any>(authCollection, logger.child({ repository: 'auth' }));
  userFirebase = CommonRepository<any>(userCollection, logger.child({ repository: 'user' }));
}
export let tasksFirebase: ICommonRepository<any>;
export let authFirebase: ICommonRepository<any>;
export let userFirebase: ICommonRepository<any>;
