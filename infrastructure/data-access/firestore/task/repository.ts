import { CustomError, Errors } from '../../../../application/entities/shared/Errors';
import { IRepositoryTask } from '../../../../application/entities/task/Task.repository';
import { ILogger } from '../../../../logger/Logger';
import { tasksFirebase } from '../config';

export const taskRepository = (logger: ILogger): IRepositoryTask => ({
  async create(task) {
    const log = logger.child({ function: 'create' });
    try {
      return await tasksFirebase.create(task);
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getById(id) {
    const log = logger.child({ function: 'getById' });
    try {
      return await tasksFirebase.getById(id);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async update(id, params) {
    const log = logger.child({ function: 'update' });
    try {
      return await tasksFirebase.update({ ...params, id });
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async delete(id) {
    const log = logger.child({ function: 'delete' });
    try {
      await tasksFirebase.removeById(id);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getAll(params) {
    const log = logger.child({ function: 'getAll' });
    try {
      return await tasksFirebase.getAll(params, ['title']);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
