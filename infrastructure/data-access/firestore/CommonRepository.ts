import { FirebaseFirestore } from '@firebase/firestore-types';
import * as admin from 'firebase-admin';

import { PaginatedResult, PaginationParams } from '../../../application/entities/common/Pagination';
import { CustomError, Errors } from '../../../application/entities/shared/Errors';
import { ILogger } from '../../../logger/Logger';
import { ICommonRepository, updateDTO } from './ICommonRepository';

function currentDateTime() {
  const currentTime = new Date();
  return currentTime.toISOString().split('T').join(' ').slice(0, -5);
}
export default <Type>(db: FirebaseFirestore.CollectionReference, logger: ILogger): ICommonRepository<Type> => ({
  db(): FirebaseFirestore.CollectionReference {
    const log = logger.child({ function: 'db' });
    try {
      log.info('function Db');
      return db;
    } catch (error: any) {
      log.error(error);
      throw new CustomError(Errors.SERVER_ERROR, error);
    }
  },
  async removeById(firebaseId: string): Promise<boolean> {
    const log = logger.child({ function: 'removeById' });
    try {
      log.info(`Delete Id: ${firebaseId}`);
      await this.getById(firebaseId);
      await db.doc(firebaseId).delete();
      return true;
    } catch (error: any) {
      log.error(error);
      if (error instanceof CustomError) {
        throw new CustomError(error.code as Errors, error.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, `Can not delete: ${error}`);
    }
  },
  async create(data: object): Promise<Type> {
    const log = logger.child({ function: 'create' });
    try {
      log.info('create data');

      const res = await db.add({
        ...cleanObject(data),
        createdAt: currentDateTime(),
        updatedAt: currentDateTime()
      });
      return this.getById(res.id);
    } catch (error) {
      log.error(error);
      throw new CustomError(Errors.SERVER_ERROR, `Can not create: ${error}`);
    }
  },
  async update(data: any | updateDTO): Promise<Type> {
    const log = logger.child({ function: 'update' });
    try {
      if (!data.id) {
        log.warn('on update fails for id');
        throw new CustomError(Errors.NOT_FOUND, `Not found ${data.id}`);
      }

      const docRef = db.doc(data.id);
      await docRef.update({
        ...data,
        updatedAt: currentDateTime()
      });
      return this.getById(data.id);
    } catch (error) {
      log.error(error);
      if (error instanceof CustomError) {
        throw new CustomError(error.code as Errors, error.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, `Can not update: ${error}`);
    }
  },
  async getById(firebaseId: string): Promise<Type> {
    const log = logger.child({ function: 'getById' });
    try {
      log.child(`get firebaseId ${firebaseId}`);
      const docRef = db.doc(firebaseId);
      const doc = await docRef.get();
      if (!doc.exists) {
        throw new CustomError(Errors.NOT_FOUND, `Not Found ${firebaseId}`);
      }
      return {
        id: doc.id,
        ...(doc.data() as Type)
      };
    } catch (error) {
      log.error(error);
      if (error instanceof CustomError) {
        throw new CustomError(error.code as Errors, error.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, `Not Found: ${error}`);
    }
  },
  async getAll<Type>(params: PaginationParams, searchColumns: string[]): Promise<PaginatedResult<Type>> {
    const log = logger.child({ function: 'getAll' });
    try {
      log.child('get all');

      const { page, limit, search } = params;
      let query: FirebaseFirestore.Query = db;

      if (search && searchColumns && searchColumns.length > 0) {
        searchColumns.forEach((column) => {
          query = query.where(column, '==', search);
        });
      }

      const totalCountSnapshot = await query.get();
      const total = totalCountSnapshot.size;
      const pages = Math.ceil(total / Number(limit));

      const start = (page - 1) * Number(limit);
      const end = start + Number(limit);

      query = query.limit(Number(limit)).offset(start);
      const snapshot = await query.get();

      const data = snapshot.docs
        .filter((doc: FirebaseFirestore.QueryDocumentSnapshot) => doc.exists)
        .map((doc) => doc.data() as Type);

      return {
        results: data,
        page,
        pages,
        total,
        limit
      };
    } catch (error) {
      log.error(error);
      throw new CustomError(Errors.SERVER_ERROR, `Not Found: ${error}`);
    }
  }
});
function cleanObject(obj: object) {
  return JSON.parse(JSON.stringify(obj));
}
