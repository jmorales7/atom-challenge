import { FirebaseFirestore } from '@firebase/firestore-types';

import { PaginatedResult, PaginationParams } from '../../../application/entities/common/Pagination';

export type updateDTO = {
  id: string;
};
export type ICommonRepository<Type> = {
  removeById(firebaseId: string): Promise<boolean>;
  create(data: object): Promise<Type>;
  getById(firebaseId: string): Promise<Type>;
  update(data: any | updateDTO): Promise<Type>;
  getAll(params: PaginationParams, searchColumns?: string[]): Promise<PaginatedResult<Type>>;
  db(): FirebaseFirestore.CollectionReference;
};
