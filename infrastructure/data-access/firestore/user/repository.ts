import bcrypt from 'bcrypt';

import { CustomError, Errors } from '../../../../application/entities/shared/Errors';
import { User } from '../../../../application/entities/user/User';
import { IRepositoryUser } from '../../../../application/entities/user/User.repository';
import { ILogger } from '../../../../logger/Logger';
import { userFirebase } from '../config';

type AuthConfig = {
  saltRounds: any;
};
export const userRepository = ({ saltRounds }: AuthConfig, logger: ILogger): IRepositoryUser => ({
  async create(user) {
    const log = logger.child({ function: 'create' });
    try {
      user.password = await bcrypt.hash(user.password, Number(saltRounds));
      return await userFirebase.create(user);
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getByEmail(email) {
    const log = logger.child({ function: 'getByRole' });
    try {
      const user = await userFirebase.db().where('email', '==', email).get();
      if (user.empty) {
        return null;
      }
      const res = user.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data()
        };
      });

      return res.shift() as User;
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
