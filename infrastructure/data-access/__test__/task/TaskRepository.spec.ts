import { taskRepository } from '../../firestore/task/repository';

describe('taskRepository', () => {
  let loggerMock: any;
  let repository: any;

  beforeEach(() => {
    loggerMock = {
      error: jest.fn()
    };

    repository = taskRepository(loggerMock);
  });

  describe('create', () => {
    it('should create a task', async () => {
      const task = {
        id: '1',
        title: 'Task 1',
        description: 'Description 1',
        createdAt: '2023-06-29 16:00:00',
        updatedAt: '2023-06-29 16:00:00'
      };

      const createMock = jest.fn().mockResolvedValue(task);

      repository.create = createMock;

      const result = await repository.create(task);

      expect(result).toEqual(task);

      expect(createMock).toHaveBeenCalledWith(task);
    });
    it('should get a task by id', async () => {
      const taskId = '1';
      const task = { id: taskId, title: 'Task 1', description: 'Description 1' };

      const getByIdMock = jest.fn().mockResolvedValue(task);
      repository.getById = getByIdMock;

      const result = await repository.getById(taskId);

      expect(result).toEqual(task);

      expect(getByIdMock).toHaveBeenCalledWith(taskId);
    });
    it('should get all tasks with pagination', async () => {
      const params = { page: 1, limit: 10, search: 'Task' };
      const tasks = [
        { id: '1', title: 'Task 1', description: 'Description 1' },
        { id: '2', title: 'Task 2', description: 'Description 2' }
      ];

      // Simular la lógica de obtención de todas las tareas
      const getAllMock = jest.fn().mockResolvedValue({
        results: tasks,
        page: params.page,
        pages: 1,
        total: tasks.length,
        limit: params.limit
      });
      repository.getAll = getAllMock;

      // Ejecutar la función que quieres probar
      const result = await repository.getAll(params);

      // Verificar el resultado
      expect(result.results).toEqual(tasks);
      expect(result.page).toBe(params.page);
      expect(result.pages).toBe(1);
      expect(result.total).toBe(tasks.length);
      expect(result.limit).toBe(params.limit);

      // Verificar que el mock se haya llamado correctamente
      expect(getAllMock).toHaveBeenCalledWith(expect.objectContaining(params));
    });
    it('should update a task', async () => {
      const taskId = '1';
      const taskData = {
        title: 'Updated Task',
        description: 'Updated Description'
      };
      const updatedTask = {
        id: taskId,
        ...taskData
      };

      const updateMock = jest.fn().mockResolvedValue(updatedTask);
      repository.update = updateMock;

      const result = await repository.update(taskId, taskData);

      // Verificar el resultado
      expect(result).toEqual(updatedTask);

      expect(updateMock).toHaveBeenCalledWith(taskId, taskData);
    });
    it('should delete a task', async () => {
      const taskId = '1';

      const removeMock = jest.fn().mockResolvedValue(true);
      repository.delete = removeMock;

      await repository.delete(taskId);

      expect(removeMock).toHaveBeenCalledWith(taskId);
    });
  });
});
