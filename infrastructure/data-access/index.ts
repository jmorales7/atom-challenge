import { IConfig } from 'config/Type';

import { ILogger } from '../../logger/Logger';
import { IDataAccess } from './IDataAccess';
import { authModule } from './auth/Model';
import { openFirebaseConnection } from './firestore/config';
import { taskRepository } from './firestore/task/repository';
import { userRepository } from './firestore/user/repository';

export const dataAccess = async ({ firebase, auth }: IConfig, loggerFactory: ILogger): Promise<IDataAccess> => {
  const logger = loggerFactory.child({ module: 'dataAccess' });
  try {
    openFirebaseConnection(firebase, logger.child({ service: 'firebase' }));
    const taskRepositorySetup = taskRepository(logger.child({ dataAccess: 'taskRepository' }));
    const authModuleSetUp = authModule(auth, logger.child({ dataAccess: 'authModule' }));
    const userRepositorySetUp = userRepository(auth, logger.child({ dataAccess: 'userRepository' }));

    return {
      taskRepository: taskRepositorySetup,
      authModule: authModuleSetUp,
      userRepository: userRepositorySetUp
    };
  } catch (e) {
    logger.error('Failed to initialize Data access with error:', e);
    throw e;
  }
};
