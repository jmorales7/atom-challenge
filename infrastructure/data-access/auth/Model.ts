import bcrypt from 'bcrypt';
import jwt, { Secret } from 'jsonwebtoken';

import { CustomError, Errors } from '../../../application/entities/shared/Errors';
import { User } from '../../../application/entities/user/User';
import { ILogger } from '../../../logger/Logger';
import { authFirebase, userFirebase } from '../firestore/config';
import { Auth } from './Auth';
import { IAuth } from './IAuth';

type AuthConfig = {
  secretJwt: string;
  saltRounds: Secret;
};
export const authModule = ({ secretJwt, saltRounds }: AuthConfig, logger: ILogger): IAuth => ({
  async create(params) {
    const log = logger.child({ function: 'create' });
    try {
      return await authFirebase.create(params);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getByRol(role) {
    const log = logger.child({ function: 'getByRol' });
    try {
      const auth = await authFirebase.db().where('role', '==', role).get();
      if (auth.empty) {
        return null;
      }
      const res = auth.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data()
        };
      });

      return res.shift() as Auth;
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },

  async update(id, params) {
    const log = logger.child({ function: 'update' });
    try {
      return await authFirebase.update({ id, ...params });
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async signIn(password, email) {
    const log = logger.child({ function: 'signIn' });
    try {
      const user = await userFirebase.db().where('email', '==', email).get();
      if (user.empty) {
        throw new CustomError(Errors.NOT_FOUND, `Not Found ${email}`);
      }
      const res = user.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data()
        };
      });

      const userFound = res.shift() as User;

      const isValid = await bcrypt.compare(password, userFound.password);
      if (!isValid) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad Credentiasl');
      return { jwt: jwt.sign({ id: userFound.id, email: userFound.email }, secretJwt, { expiresIn: '1h' }) };
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async identify(token, permission) {
    const log = logger.child({ function: 'identify' });
    try {
      const decoded: any = await jwt.verify(token, secretJwt);
      const user = await userFirebase.db().where('email', '==', decoded.email).get();
      if (user.empty) {
        throw new CustomError(Errors.NOT_FOUND, `Not Found ${decoded.email}`);
      }
      const res = user.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data()
        };
      });

      const userFound = res.shift() as User;
      const role = await this.getByRol(userFound.role);
      return role?.permissions.includes(permission);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
