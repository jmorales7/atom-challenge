import { IRepositoryTask } from '../../application/entities/task/Task.repository';
import { IRepositoryUser } from '../../application/entities/user/User.repository';
import { IAuth } from './auth/IAuth';

export type IDataAccess = {
  taskRepository: IRepositoryTask;
  authModule: IAuth;
  userRepository: IRepositoryUser;
};
