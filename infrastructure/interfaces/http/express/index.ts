import cors from 'cors';
import express from 'express';
import { NextFunction, Request, RequestHandler, Response } from 'express-serve-static-core';
import pinoHttp from 'pino-http';
import stream from 'stream';

import { ILogger } from '../../../../logger/Logger';
import { schemaValidation } from '../middlewares/SchemaValidation';
import { ResponseMapper } from '../presenters/Results';
import { Route } from '../routes';

const httpLogger = pinoHttp();
const expressApp = express();
expressApp.use(httpLogger);
expressApp.use(cors());
expressApp.set('trust proxy', true); //avoid nginx proxy ip and get actual client ip in req.op
expressApp.use(express.json());
expressApp.use(express.urlencoded({ extended: false }));

const addRouteConfig =
  (route: any) =>
  (req: Request, res: Response, next: NextFunction): void => {
    req.route.extraConfig = route;
    next();
  };

function extractParams(req: Request) {
  const actorLocation = {
    ip: req.ip,
    userAgent: req.headers['user-agent'],
    originDomain: req.headers.origin
  };
  const identificatorObject = Object.assign(req.headers, { actorLocation });
  return Object.assign(
    { identificatorObject },
    { body: req.body },
    req.query,
    req.params,
    { file: req.file },
    { files: req.files }
  );
}

const errorHandler =
  (mapper: ResponseMapper, logger: ILogger) => async (error: any, req: Request, res: Response, next: any) => {
    const { status, data } = mapper(error);
    res.status(status).json(data);
  };

const responseHandler = (mapper: ResponseMapper, logger: ILogger, route: Route) => async (req: any, res: any) => {
  const { status, data } = mapper(req.results);
  if (route.fileBuffer) {
    try {
      const readStream = new stream.PassThrough();
      readStream.end(data.buffer);
      res.set('Content-disposition', 'attachment; filename=' + data.filename);
      res.set('Content-Type', 'application/octet-stream');
      readStream.pipe(res);
    } catch (error) {
      logger.error('Failed to stream private file with error:', error);
    }
  } else {
    res.status(status).json(data);
  }
};

const endpoint = (uoc: any, route: Route, logger: ILogger) => async (req: any, res: any, next: any) => {
  try {
    req.log.info();
    req.results = await uoc(extractParams(req));
    next();
  } catch (error) {
    next(error);
  }
};

export const server = (
  routes: Array<Route> = [],
  responseMapper: (code?: number) => ResponseMapper,
  errorMapper: ResponseMapper,
  port: number,
  middlewares: Array<RequestHandler> = [],
  baseLogger: ILogger
) => {
  const logger = baseLogger.child({ module: 'express' });
  routes.forEach((route: Route) => {
    const path = route.path;
    const handlers = [
      addRouteConfig(route),
      ...middlewares,
      schemaValidation(route.schemaValidation),
      endpoint(route.useCase, route, baseLogger.child({ middleware: 'endpoint' })),
      errorHandler(errorMapper, baseLogger.child({ middleware: 'errorHandler' })),
      responseHandler(responseMapper(route.successCode), baseLogger.child({ middleware: 'responseHandler' }), route)
    ];
    switch (route.verb.toLowerCase()) {
      case 'get':
        expressApp.get(path, handlers);
        break;
      case 'post':
        expressApp.post(path, handlers);
        break;
      case 'put':
        expressApp.put(path, handlers);
        break;
      case 'delete':
        expressApp.delete(path, handlers);
        break;
      case 'patch':
        expressApp.patch(path, handlers);
        break;
    }
  });

  const server = expressApp.listen(port);
  logger.info({ port }, 'Server running');
  return server;
};
