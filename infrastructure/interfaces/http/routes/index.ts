import { AnyZodObject } from 'zod';

import { CreateAuthDTO, GetByRoleDTO, UpdateAuthDTO } from '../../../../application/entities/auth/Auth.dto';
import {
  CreateTaskDTO,
  GetAllTasksDTO,
  GetByIdTaskDTO,
  UpdateTaskDTO
} from '../../../../application/entities/task/Task.dto';
import { CreateUserDTO } from '../../../../application/entities/user/User.dto';

export type Route = {
  path: string;
  verb: string;
  useCase: any;
  successCode?: number;
  fileBuffer?: boolean;
  schemaValidation?: AnyZodObject | undefined;
};

export const routes: (dependencies: any) => Array<Route> = (dependencies: any) => [
  { path: '/tasks', verb: 'POST', useCase: dependencies.createTask, schemaValidation: CreateTaskDTO },
  { path: '/tasks/:taskId', verb: 'GET', useCase: dependencies.getByIdTask, schemaValidation: GetByIdTaskDTO },
  { path: '/tasks/:taskId', verb: 'PUT', useCase: dependencies.updateTask, schemaValidation: UpdateTaskDTO },
  { path: '/tasks/:taskId', verb: 'DELETE', useCase: dependencies.deleteTask, schemaValidation: GetByIdTaskDTO },
  { path: '/tasks', verb: 'GET', useCase: dependencies.getAllTask, schemaValidation: GetAllTasksDTO },

  { path: '/auth', verb: 'POST', useCase: dependencies.createAuth, schemaValidation: CreateAuthDTO },
  { path: '/auth/:role', verb: 'GET', useCase: dependencies.getByRole, schemaValidation: GetByRoleDTO },
  { path: '/auth/:role', verb: 'PUT', useCase: dependencies.updateAuth, schemaValidation: UpdateAuthDTO },

  { path: '/users', verb: 'POST', useCase: dependencies.createUser, schemaValidation: CreateUserDTO },
  { path: '/sign-in', verb: 'POST', useCase: dependencies.signIn }
];
