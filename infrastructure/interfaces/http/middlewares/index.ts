import { RequestHandler } from 'express-serve-static-core';

import { attachmentParser } from './AttachmentsParser';

export const middlewares = (): Array<RequestHandler> => [attachmentParser];
