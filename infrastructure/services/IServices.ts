import { IAuthEntity } from '../../application/entities/auth/IAuth';
import { ITask } from '../../application/entities/task/ITask';
import { IUser } from '../../application/entities/user/IUser';

export type IServices = {
  taskService: ITask;
  authService: IAuthEntity;
  userService: IUser;
};
