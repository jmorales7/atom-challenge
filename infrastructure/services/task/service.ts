import { ITask } from '../../../application/entities/task/ITask';
import { ILogger } from '../../../logger/Logger';
import { IDataAccess } from '../../data-access/IDataAccess';

export const taskService = ({ taskRepository }: IDataAccess, logger: ILogger): ITask => ({
  async create(params) {
    return await taskRepository.create(params);
  },
  async getById(id) {
    return await taskRepository.getById(id);
  },
  async update(id, params) {
    return await taskRepository.update(id, params);
  },
  async delete(id) {
    await taskRepository.delete(id);
  },
  async getAll(params) {
    return await taskRepository.getAll(params);
  }
});
