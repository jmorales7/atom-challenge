import { IDataAccess } from 'infrastructure/data-access/IDataAccess';

import { ILogger } from '../../logger/Logger';
import { IServices } from './IServices';
import { authService } from './auth/service';
import { taskService } from './task/service';
import { userService } from './user/service';

export const services = (dataAccess: IDataAccess, loggerFactory: ILogger, preInjection: boolean): IServices => {
  const logger = loggerFactory.child({ module: 'services' });
  logger.info('starting services');
  try {
    const taskServiceSetUp = taskService(dataAccess, logger);
    const authServiceSetUp = authService(dataAccess, logger);
    const userServiceSetUp = userService(dataAccess, logger);
    preInjection ? logger.info('Dependencies pre-injection done') : logger.info('Dependencies fully injected');
    return {
      taskService: taskServiceSetUp,
      authService: authServiceSetUp,
      userService: userServiceSetUp
    };
  } catch (e) {
    logger.error(e, 'failed on start services');
    throw e;
  }
};
