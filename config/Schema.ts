import Joi from 'joi';

export const configSchema = Joi.object({
  http: {
    port: Joi.string()
  },
  baseLogger: Joi.any(),
  firebase: {
    file: Joi.string()
  },
  auth: {
    secretJwt: Joi.string(),
    saltRounds: Joi.any()
  }
});
