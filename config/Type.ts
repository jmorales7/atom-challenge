import { ILogger } from '../logger/Logger';

export type IConfig = {
  http: {
    port: number;
  };
  baseLogger: ILogger;
  firebase: {
    file: string;
  };
  auth: {
    secretJwt: string;
    saltRounds: any;
  };
};
