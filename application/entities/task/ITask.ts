import { PaginatedResult, PaginationParams } from '../common/Pagination';
import { Task } from './Task';
import { CreateTaskDTO, UpdateTaskDTO } from './Task.dto';

export type ITask = {
  create(params: CreateTaskDTO): Promise<Task>;
  getById(id: string): Promise<Task>;
  update(id: string, params: UpdateTaskDTO): Promise<Task>;
  delete(id: string): Promise<void>;
  getAll(params: PaginationParams): Promise<PaginatedResult<Task>>;
};
