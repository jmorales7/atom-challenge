import { z } from 'zod';

export const CreateTaskDTO = z
  .object({
    title: z.string(),
    description: z.string()
  })
  .strict();

export type CreateTaskDTO = z.infer<typeof CreateTaskDTO>;

export const UpdateTaskDTO = z
  .object({
    taskId: z.string(),
    title: z.string().optional(),
    description: z.string().optional()
  })
  .strict();

export type UpdateTaskDTO = z.infer<typeof UpdateTaskDTO>;

export const GetByIdTaskDTO = z
  .object({
    taskId: z.string()
  })
  .strict();

export type GetByIdTaskDTO = z.infer<typeof GetByIdTaskDTO>;

export const GetAllTasksDTO = z
  .object({
    limit: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
    page: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
    search: z.string().optional()
  })
  .strict();
export type GetAllTasksDTO = z.infer<typeof GetAllTasksDTO>;
