import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { Task } from '../../../entities/task/Task';
import { UpdateTaskDTO } from '../../../entities/task/Task.dto';

type Payload = {
  body: UpdateTaskDTO;
  taskId: string;
  identificatorObject: any;
};
export const updateTask =
  ({ taskService, authService }: IServices, logger: ILogger) =>
  async ({ taskId, body, identificatorObject }: Payload): Promise<Task> => {
    logger.info('UOC-updateTaskTask');
    const hasPermission = await authService.identify(identificatorObject.authorization?.split(' ')[1], 'updateTask');
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await taskService.update(taskId, body);
  };
