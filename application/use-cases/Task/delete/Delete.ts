import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';

type Payload = {
  taskId: string;
  identificatorObject: any;
};
export const deleteTask =
  ({ taskService, authService }: IServices, logger: ILogger) =>
  async ({ taskId, identificatorObject }: Payload): Promise<void> => {
    logger.info('UOC-deleteTask');
    const hasPermission = await authService.identify(identificatorObject.authorization?.split(' ')[1], 'deleteTask');
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    await taskService.delete(taskId);
  };
