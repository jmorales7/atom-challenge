import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { CreateTaskDTO } from '../../../entities/task/Task.dto';
import {Task} from "../../../entities/task/Task";

type Payload = {
  body: CreateTaskDTO;
  identificatorObject: any;
};
export const createTask =
  ({ taskService, authService }: IServices, logger: ILogger) =>
  async ({ body, identificatorObject }: Payload): Promise<Task> => {
    logger.info('UOC-createTask');
    const hasPermission = await authService.identify(identificatorObject.authorization?.split(' ')[1], 'createTask');
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await taskService.create(body);
  };
