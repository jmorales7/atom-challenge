import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { PaginatedResult } from '../../../entities/common/Pagination';
import { Task } from '../../../entities/task/Task';
import { GetAllTasksDTO } from '../../../entities/task/Task.dto';

export const getAllTask =
  ({ taskService }: IServices, logger: ILogger) =>
  async ({ page, limit, search }: GetAllTasksDTO): Promise<PaginatedResult<Task>> => {
    logger.info('UOC-getAllTask');
    return await taskService.getAll({ page, limit, search });
  };
