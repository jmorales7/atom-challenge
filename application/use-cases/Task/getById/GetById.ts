import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { Task } from '../../../entities/task/Task';

type Payload = {
  taskId: string;
  identificatorObject: any;
};
export const getByIdTask =
  ({ taskService, authService }: IServices, logger: ILogger) =>
  async ({ taskId, identificatorObject }: Payload): Promise<Task> => {
    logger.info('UOC-getByIdTask');
    const hasPermission = await authService.identify(identificatorObject.authorization?.split(' ')[1], 'getByIdTask');
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await taskService.getById(taskId);
  };
