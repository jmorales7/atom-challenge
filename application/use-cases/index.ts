import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../logger/Logger';
import { createAuth } from './Auth/create/Create';
import { getByRole } from './Auth/getByRole/GetByRole';
import { updateAuth } from './Auth/update/Update';
import { createTask } from './Task/create/Create';
import { deleteTask } from './Task/delete/Delete';
import { getAllTask } from './Task/getAll/GetAll';
import { getByIdTask } from './Task/getById/GetById';
import { updateTask } from './Task/update/Update';
import { createUser } from './User/create/Create';
import { signIn } from './User/signIn/SignIn';

export const useCaseFactory = (services: IServices, baseLogger: ILogger) => ({
  createTask: createTask(services, baseLogger.child({ controller: 'createTask' })),
  getByIdTask: getByIdTask(services, baseLogger.child({ controller: 'getByIdTask' })),
  updateTask: updateTask(services, baseLogger.child({ controller: 'updateTask' })),
  deleteTask: deleteTask(services, baseLogger.child({ controller: 'deleteTask' })),
  getAllTask: getAllTask(services, baseLogger.child({ controller: 'getAllTask' })),
  createAuth: createAuth(services, baseLogger.child({ controller: 'createAuth' })),
  getByRole: getByRole(services, baseLogger.child({ controller: 'getByRole' })),
  updateAuth: updateAuth(services, baseLogger.child({ controller: 'updateAuth' })),
  createUser: createUser(services, baseLogger.child({ controller: 'createUser' })),
  signIn: signIn(services, baseLogger.child({ controller: 'signIn' }))
});
