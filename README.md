# Challenge - TS & Clean Architecture boilerplate - Task

## Juan Pablo Morales Rodriguez

This project was built thinking in a clean architecture and consuming the bitfinex api services to solve the needs of the challenge.

- ✨Magic ✨

## Features

- Typescript
- Node Js
- Clean Architecture
- Test
- Express Js
- Work environment ready to build any type of backend
- JWT
- bcrypt
- authentication and authorization

## Implementation

> For the implementation of this project, we implemented a clean architecture where we seek that each layer of the project has certain responsibilities and thus can be more readable and scalable, we added our own method of authentication and authorization where we protect some routes and thus have a more secure project, we implemented different technologies to have a project that is prepared to receive files, authenticate routes and each layer is responsible for its own logistics, without affecting the business model.

## Commands

Install the dependencies and devDependencies and start the server to run in a local environment.

```sh
yarn
yarn prepare
yarn dev
```

Make the code aesthetic

```sh
yarn prettier:fix
```

Other commands are executed with the pre-commit.

you can find an insomnia in.

```sh
insomnia in infraestructure/interfaces/http/docs
```

superAdmin credentials.

```sh
email: superAdmin@gmail.com
password: admin
```
user credentials.

```sh
email: user@gmail.com
password: user
```

## URL production

```sh
https://gateway-aiol.onrender.com
```

## More Details

> see the video for more details.
